/**
 * New node file
 */
module.exports.ENGLISH				= "en";
module.exports.SYSTEM_USER			= "SystemUser";
module.exports.ADMIN_CREDENTIALS	= "AdminCred";
module.exports.PROXY_COLLECTION		= "proxies";
module.exports.USER_COLLECTION		= "users";